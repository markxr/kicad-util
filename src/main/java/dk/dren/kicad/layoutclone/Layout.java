package dk.dren.kicad.layoutclone;

import java.util.List;

public interface Layout {
    List<LayoutOffset> getOffsets();
}
