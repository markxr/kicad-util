package dk.dren.kicad.textops;

import dk.dren.kicad.pcb.FpText;

import java.util.function.Consumer;

public interface TextOperation extends Consumer<FpText> {
}
