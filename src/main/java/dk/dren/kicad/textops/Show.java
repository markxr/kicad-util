package dk.dren.kicad.textops;

import dk.dren.kicad.pcb.FpText;
import dk.dren.kicad.pcb.MutableValue;
import dk.dren.kicad.pcb.NodeOrValue;

public class Show implements TextOperation {

    @Override
    public void accept(FpText fpText) {
        fpText.alterChildren(ch->{
            NodeOrValue possiblyHide = ch.get(Hide.INDEX_OF_HIDE);
            if (possiblyHide instanceof MutableValue && ((MutableValue)possiblyHide).getValue().equals(Hide.HIDE)) {
                ch.remove(Hide.INDEX_OF_HIDE);
            }
        });
    }
}
