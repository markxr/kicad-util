package dk.dren.kicad.textops;

import dk.dren.kicad.pcb.FpText;
import dk.dren.kicad.pcb.Position;
import dk.dren.kicad.primitive.Point;
import org.decimal4j.immutable.Decimal6f;

public class MoveAwayFromOrigin implements TextOperation {

    private final Decimal6f xo;
    private Decimal6f yo;

    public MoveAwayFromOrigin(double offset) {
        if (offset == 0) {
            xo = yo = Decimal6f.ZERO;
        } else {
            xo = Decimal6f.valueOf(-0.14);
            yo = Decimal6f.valueOf(offset);
        }
    }

    @Override
    public void accept(FpText fpText) {
        Position pos = fpText.getPosition();
        Point point = pos.getXyr();
        point.setX(xo);
        point.setY(yo);
    }
}
