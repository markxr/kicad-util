package dk.dren.kicad;

import dk.dren.kicad.layoutclone.ArrayLayout;
import dk.dren.kicad.layoutclone.Layout;
import dk.dren.kicad.layoutclone.LayoutOffset;
import dk.dren.kicad.pcb.*;
import dk.dren.kicad.primitive.ModuleReference;
import lombok.extern.java.Log;
import org.decimal4j.immutable.Decimal6f;

import java.io.*;
import java.util.*;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

@Log
public class Foo {

    public static final String CMTS_USER = "Cmts.User";

    public static void main(String[] argv) {
        Board board;
        try {
            board = KicadPcbSerializer.read(new File(argv[0]));
        } catch (Exception e) {
            log.log(Level.SEVERE, "failed to read "+argv[0], e);
            System.exit(1);
            return;
        }


        try {
            Optional<Zone> templateZone = board.zones().filter(z -> z.getLayer().equals(CMTS_USER)).findFirst();
            if (!templateZone.isPresent()) {
                throw new IllegalArgumentException("Could not find a zone on the " + CMTS_USER + " layer");
            }
            Zone zone = templateZone.get();



            List<dk.dren.kicad.pcb.Module> templateModules = board.modules().filter(m -> zone.getPolygon().contains(m)).collect(Collectors.toList());
/*
            Set<String> as = new TreeSet<>();
            as.add("U501");
            as.add("U901");
            as.add("U401");
            as.add("U301");

            as.add("U801");
            as.add("U601");
            as.add("U201");
            as.add("U701");

            Layout layout = new AnchoredLayout(board, templateModules, as);
*/

            List<Integer> as = new ArrayList<>();
            as.add(400);
            as.add(-100);
            as.add(-200);

            as.add(300);
            as.add(100);
            as.add(-300);
            as.add(200);
            Layout layout = new ArrayLayout(as, Decimal6f.valueOf(10));

            templateModules.forEach(templateModule -> {
                for (LayoutOffset offset : layout.getOffsets()) {
                    ModuleReference referenceToMove = templateModule.getReference().add(offset.getReference());
                    dk.dren.kicad.pcb.Module moduleToMove = board.getModuleWithReference(referenceToMove);
                    if (moduleToMove == null) {
                        throw new IllegalArgumentException("Didn't find a module with reference "+referenceToMove+" needed because module "+templateModule.getReference()+" is in the template");
                    }
                    moduleToMove.getPosition().setXyr(templateModule.getPosition().getXyr().add(offset.getXyr()));
                    moduleToMove.cloneLayerAndStyle(templateModule);
                }
            });

/*
            if (tainting) {
                board.vias().filter(via -> ((Position) via.getProperty(Position.AT)).isTainted())
                        .forEach(board::removeChild);
                board.segments().filter(seg -> ((Position) seg.getProperty(Position.START)).isTainted() || ((Position) seg.getProperty(Position.END)).isTainted())
                        .forEach(board::removeChild);
            }
*/
            // Clone all vias in the template zone, move them the offset and remove the net information
            board.vias()
                    .filter(via -> zone.getPolygon().containsNode(via, Position.AT))
                    .forEach(templateVia-> {

                        layout.getOffsets().forEach(offset -> {
                            RawNode via = KicadPcbSerializer.clone(templateVia);

                            Position pos = (Position) via.getProperty(Position.AT);
                            pos.setXyr(pos.getXyr().add(offset.getXyr()));

                            via.removeChild(via.getProperty("net"));

                            board.addChild(via);
                        });
                    });

            // Clone all the tracks in the template zone, move them the offset and remove the net information
            board.segments()
                    .filter(segment -> zone.getPolygon().containsNode(segment, Position.START) || zone.getPolygon().containsNode(segment, Position.END))
                    .forEach(templateSegment-> {

                        layout.getOffsets().forEach(offset -> {
                            RawNode segment = KicadPcbSerializer.clone(templateSegment);

                            Position start = (Position) segment.getProperty(Position.START);
                            start.setXyr(start.getXyr().add(offset.getXyr()));

                            Position end = (Position) segment.getProperty(Position.END);
                            end.setXyr(end.getXyr().add(offset.getXyr()));

                            segment.removeChild(segment.getProperty("net"));

                            board.addChild(segment);
                        });
                    });

        } catch (Exception e) {
            log.log(Level.SEVERE, "failed to transform the board", e);
            System.exit(2);
            return;
        }

        try (Writer writer = new FileWriter(new File(argv[1]))) {
            KicadPcbSerializer.write(board, writer);
        } catch (Exception e) {
            log.log(Level.SEVERE, "failed to write "+argv[1], e);
            System.exit(3);
            return;
        }


    }
}



