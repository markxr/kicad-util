package dk.dren.kicad.cmd;

import dk.dren.kicad.pcb.LineNodeIntersection;
import dk.dren.kicad.pcb.*;
import dk.dren.kicad.pcb.Vector;
import dk.dren.kicad.primitive.Point;
import org.decimal4j.immutable.Decimal6f;
import picocli.CommandLine;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

@CommandLine.Command(name = "panel", description = "Creates mouse-bites out of Eco1.User lines where they cross Edge.Cuts")
public class PanelStitching implements Callable<Integer> {
    public static final double CURVE_SEGMENT = 0.1;

    @CommandLine.ParentCommand
    PCBCmd pcbCmd;

    @CommandLine.Option(
            names = {"--width"},
            description = "The distance from the middle of the tab to the place where the routing fillet starts,\n" +
                    "Default is 2.5 mm, if the fillet radius is 1 mm, then you'll end up with a 3 mm wide tab and\n" +
                    "5 mm from flat edge to flat edge at the root of the mouse-bites"
    )
    Decimal6f width = Decimal6f.valueOf(2.5);

    @CommandLine.Option(
            names = {"--hole"},
            description = "The diameter of the mouse-bite holes,\n" +
                    "Default is 0.8 mm"
    )
    Decimal6f holeDiameter = Decimal6f.valueOf(0.8);

    @CommandLine.Option(
            names = {"--inset"},
            description = "The distance from the edge of the board to the center of the mouse-bite holes,\n" +
                    "Default is 0.25 mm"
    )
    Decimal6f holeInset = Decimal6f.valueOf(0.25);

    @CommandLine.Option(
            names = {"--pitch"},
            description = "The center distance between mouse-bite holes to attempt hitting,\n" +
                    "Default is 1.3 mm"
    )
    Decimal6f holePitch = Decimal6f.valueOf(1.3);

    @CommandLine.Option(
            names = {"--fillet"},
            description = "The radius of the routing bit, a larger fillet will cause a thinner tab for the same width,\n" +
                    "Default is 1.3 mm to accommodate the typical 2.5 mm router bit without hitting both PCBs"
    )
    Decimal6f filletRadius = Decimal6f.valueOf(1.3);


    public static final Decimal6f EDGE_CUT_LINE_WIDTH = Decimal6f.valueOf(0.1);

    @Override
    public Integer call() throws Exception {
        int stitchCount = 0;
        Board board = pcbCmd.getBoard();

        Map<Point, Node> viaByPosition = new TreeMap<>();
        board.vias().forEach(via->viaByPosition.put(Position.ofVia(via), via));

        for (LineNode stitch : board.eco1UserLines().collect(Collectors.toList())) {

            Node startVia = viaByPosition.get(stitch.getEnds().getStart());
            Node endVia   = viaByPosition.get(stitch.getEnds().getEnd());

            Point mid = stitch.getEnds().getMidPoint();

            StitchBite first = halfStitch(mid, stitch.getEnds().getStart(), startVia != null, stitchCount++);
            StitchBite second = halfStitch(mid, stitch.getEnds().getEnd(), endVia != null, stitchCount++);

            addPath(first.getA(), first.getDirection(), second.getB(), second.getDirection().normal().normal());
            addPath(first.getB(), first.getDirection().normal().normal(), second.getA(), second.getDirection());

            board.removeChild(stitch);
            if (startVia != null) {
                board.removeChild(startVia);
            }
            if (endVia != null) {
                board.removeChild(endVia);
            }
        }

        for (LineNode breakLine : board.lines(LineNode.ECO2_USER).collect(Collectors.toList())) {
            addHoles(breakLine.getEnds().getStart(), breakLine.getEnds().getEnd());
            board.removeChild(breakLine);
        }

        try (Writer writer = new FileWriter(new File(pcbCmd.getPcbFile().getParentFile(), "output."+pcbCmd.getPcbFile().getName()))) {
            KicadPcbSerializer.write(board, writer);
        }

        return 0;
    }

    /**
     * Creates a path from point a, starting with direction av and b, ending with direction bv
     * with a minimum bending radios of filletRadius
     *
     * Note that the line segments generated are not in any reasonable order, but they do intersect at end-points.z
     *
     * @param a The starting point
     * @param av The starting direction vector
     * @param b The ending point
     * @param bv The ending direction vector
     */
    private void addPath(Point a, Vector av, Point b, Vector bv) {
        double stepLength = bv.length().doubleValue();
        double stepAngle = Math.PI/2-Math.acos(Math.pow(stepLength,2) / (2 * filletRadius.doubleValue() * stepLength));
        stepAngle *= 2;
        int patience = 100;

        while (patience-- > 0) {
            double adiff = av.angle(Vector.between(a, b));
            double bdiff = bv.angle(Vector.between(b, a));

            if (Math.abs(adiff) < stepAngle && Math.abs(bdiff) < stepAngle) {
                addLine(a,b);
                return;

            } else if (Math.abs(adiff) > Math.abs(bdiff)) {
                // A is more off, so adjust that.
                av = av.rotate(-Math.signum(adiff) * Math.min(stepAngle, Math.abs(adiff)));

                Point newA = av.offset(a);
                addLine(a, newA);
                a = newA;

            } else {
                // B is more off
                bv = bv.rotate(-Math.signum(bdiff) * Math.min(stepAngle, Math.abs(bdiff)));
                Point newB = bv.offset(b);
                addLine(b, newB);
                b = newB;
            }
        }
    }

    /**
     * Create the stitch starting at the point outside the board towards the point inside the board.
     * @param outside The point outside the boards
     * @param inside The point inside one board.
     * @param noHoles
     */
    private StitchBite halfStitch(Point outside, Point inside, boolean noHoles, int stitchCount) {
        // First find all the line segments that intersect the left and right edges of the stitch
        // For each of them split at the intersection point.
        final Vector tabDirection = Vector.between(outside, inside).unit();
        final Vector tabNormal = tabDirection.normal();
        final LineNodeIntersection eci = pcbCmd.getBoard().findEdgeCutIntersecting(outside, inside);
        if (eci == null) {
            throw new IllegalArgumentException("Could not find an edge cut line that intersects the "+LineNode.ECO1_USER+" line from "+outside+" to "+inside);
        }

        List<Point> points = takeBite(eci);

        final double dotProductDeterminingIfBiteShouldBeReversed = Vector.between(eci.getIntersection(), points.get(0)).dot(tabNormal);
        if (dotProductDeterminingIfBiteShouldBeReversed < 0) {
            Collections.reverse(points);
        }

        Point edgeA = points.get(0);
        Point edgeB = points.get(points.size()-1);
        Vector biteDirection = Vector.between(edgeA, edgeB).unit();
        Vector biteNormal = biteDirection.normal();

        // Create the holes
        Vector inset = biteNormal.multiply(holeInset);
        double snuggy;
        Decimal6f holeRadius = holeDiameter.divide(Decimal6f.TWO);
        if (holeInset.isGreaterThanOrEqualTo(holeRadius)) {
            snuggy = 0;
        } else {
            snuggy = Math.sqrt(Math.pow(holeRadius.doubleValue(), 2) - Math.pow(holeInset.doubleValue(), 2));
        }
        Vector aSnug = biteDirection.multiply(snuggy);
        Vector bSnug = biteDirection.multiply(-snuggy);

        if (!noHoles) {
            addHoles(inset.add(aSnug).offset(edgeA), inset.add(bSnug).offset(edgeB));
            addCourtYard(edgeA, edgeB, 3, "All");
            addCourtYard(edgeA, edgeB, 6, "MLCC");
        }

        return new StitchBite(edgeA, edgeB, biteDirection.unit().multiply(CURVE_SEGMENT));
    }

    private List<Point> takeBite(LineNodeIntersection eci) {
        // The first thing we do is to remove the edge cut that was intersected, because we know that it will not be
        // needed after we're done, the ends that might poke outside of the bite will be created by the crawl methods
        pcbCmd.getBoard().removeChild(eci.getLineNode());

        // Note the bite list is the points that the bite passes through from the "start" side of the initial
        // edge cut intersected by the stitch line to the "end" side of the cut line.
        List<Point> bite = new ArrayList<>();

        bite.addAll(crawl(Decimal6f.ZERO, eci.getIntersection(), eci.getLineNode().getEnds().getStart()));
        Collections.reverse(bite); // Because we want the first point in the list to be at the edge, not the middle.
        bite.addAll(crawl(Decimal6f.ZERO, eci.getIntersection(), eci.getLineNode().getEnds().getEnd()));

        return bite;
    }

    /**
     * Crawl along lines and remove them from the document as we go, once one is found where the end point further away
     * from the middle point than the width limit, a replacement Edge.Cut will be created from the end point to the
     * point on the line that is the exact width away from the middle point.
     *
     * @param distanceFromMiddleAtStart The distance from the middle point to the start point
     * @param start The starting point of this step
     * @param end The end point of this step
     * @return The points traversed, excluding the start point
     */
    private List<Point> crawl(Decimal6f distanceFromMiddleAtStart, Point start, Point end) {
        List<Point> points = new ArrayList<>();
        Vector startToEnd = Vector.between(start, end);
        Decimal6f distanceFromMiddleAtEnd = distanceFromMiddleAtStart.add(startToEnd.length());

        if (distanceFromMiddleAtEnd.isGreaterThanOrEqualTo(width)) {
            Decimal6f distanceFromStartToEdge = width.subtract(distanceFromMiddleAtStart);
            Point edgePoint = startToEnd.unit().multiply(distanceFromStartToEdge).offset(start);

            addLine(edgePoint, end);
            points.add(edgePoint);

        } else {

            LineNodeIntersection connectingLine = pcbCmd.getBoard().findEdgeCutLineConnectingTo(end);

            if (connectingLine == null) {
                throw new IllegalArgumentException("Could not find an Edge.Cut line connecting to "+end+" to continue the line from "+start+" to "+end);
            }

            pcbCmd.getBoard().removeChild(connectingLine.getLineNode());
            points.add(end);

            Point farEnd = connectingLine.getIntersection();
            points.addAll(crawl(distanceFromMiddleAtEnd, end, farEnd));
        }
        return points;
    }

    /**
     * Add a counter-clockwise circular arc with the center at center from the start point to the end point.
     *
     * The radius is defined by the average of the distances between center and start and center and end.
     *
     * Any inaccuracy is taken up by the first and last segment to ensure that the lines end up anchored exactly
     * at the start and end points at each end.
     *  @param center The center of the arc
     * @param start The starting point of the arc
     * @param end The ending point of the arc
     * @param radius The radius of the arc
     */
    private void addArc(Point center, Point start, Point end, double angle, double radius, String layer) {
        Vector sv = Vector.between(center, start);

        double maximumSegmentAngle = Math.acos(2 * Math.pow(1 - 0.01 / radius, 2) - 1);

        Vector radiusVector = sv.unit().multiply(radius);

        int segments = Math.max((int)Math.ceil(angle / maximumSegmentAngle) - 1, 6);
        double segmentAngle = angle/segments;

        Point prev = start;
        for (int i=1;i<segments;i++) {
            double ia = i*segmentAngle;
            Point p = radiusVector.rotate(ia).offset(center);
            addLine(prev, p, layer);
            prev = p;
        }

        addLine(prev, end, layer);
    }

    private void addLine(Point a, Point b) {
        addLine(a, b, LineNode.EDGE_CUTS);
    }

    private void addLine(Point a, Point b, String layer) {
        addLine(a, b, layer, EDGE_CUT_LINE_WIDTH);
    }

    private void addLine(Point a, Point b, String layer, double width) {
        addLine(a, b, layer, Decimal6f.valueOf(width));
    }

    private void addLine(Point a, Point b, String layer, Decimal6f width) {
        pcbCmd.getBoard().addChild(new LineNode(new Line(a, b), layer, width, null));
    }


    private void addHoles(Point a, Point b) {
        Vector aToB = Vector.between(a, b);
        Decimal6f length = aToB.length();

        int spaces = length.divideBy(holePitch).round(0).intValue();
        Vector abStep = aToB.unit().multiply(length.divide(spaces));

        RawNode rn = new RawNode(dk.dren.kicad.pcb.Module.NAME, new MutableValue("holes"));

        rn.addChild(new RawNode("layer", new MutableValue("F.Cu")));
        rn.addChild(new Position(Position.AT, a));
        rn.addChild(new RawNode("tedit", new MutableValue("1")));
        rn.addChild(new RawNode("tstamp", new MutableValue("1")));
        rn.addChild(new RawNode("attr", new MutableValue("virtual")));

        MutableValue hole = new MutableValue(holeDiameter);
        for (int i=0;i<=spaces;i++) {
            RawNode pad = new RawNode("pad",
                    new MutableValue("h"+i),
                    new MutableValue("np_thru_hole"),
                    new MutableValue("circle"),
                    new Position(Position.AT, abStep.multiply(Decimal6f.valueOf(i)).toPoint()),
                    new RawNode("size", hole, hole),
                    new RawNode("drill", hole),
                    new RawNode("layers", new MutableValue("*.Cu"), new MutableValue("*.Mask"))
            );
            rn.addChild(pad);

        /*
        (pad "" np_thru_hole circle (at 0 0) (size 0.8 0.8) (drill 0.8) (layers *.Cu *.Mask))
        */
        }

        pcbCmd.getBoard().addChild(rn);
    }

    private void addCourtYard(Point a, Point b, double distance, String label) {
        String layer = LineNode.FRONT_FABRICATION;

        Vector aToB = Vector.between(a, b);
        Vector abUnit = aToB.unit();
        Vector in = abUnit.normal();

        Point ae = abUnit.multiply(-distance).offset(a);
        Point ai = in.multiply(distance).offset(a);
        addArc(a, ae, ai, Math.PI/2, distance, layer);

        Point be = abUnit.multiply(distance).offset(b);
        Point bi = in.multiply(distance).offset(b);
        addArc(b, bi, be, Math.PI/2, distance, layer);

        addLine(ai,bi, layer);
        addLine(ae,be, layer);

        addText(abUnit.multiply(aToB.length().doubleValue()/2).add(in.multiply(distance-0.6)).offset(a), label, layer);
    }

    private void addText(Point point, String text, String layer) {
        RawNode grText = new RawNode("gr_text",
                new MutableValue(text),
                new Position(Position.AT, point),
                new RawNode("layer", new MutableValue(layer)),

                new RawNode("effects",
                        new RawNode("font",
                                new RawNode("size", new MutableValue("0.5"), new MutableValue("0.5")),
                                new RawNode("thickness", new MutableValue("0.125")))
                        )
                );
        pcbCmd.getBoard().addChild(grText);
        /*
        (gr_text tap1 (at 33.725 48.875) (layer F.SilkS)
          (effects (font (size 0.5 0.5) (thickness 0.125)))
        )
        */
    }

    /*
     (zone (net 0) (net_name "") (layers F.Cu 2_Ground 3_Misc B.Cu) (tstamp 0) (hatch edge 0.508)
    (connect_pads (clearance 0.2))
    (min_thickness 0.254)
    (keepout (tracks not_allowed) (vias not_allowed) (copperpour not_allowed))
    (fill (arc_segments 16) (thermal_gap 0.508) (thermal_bridge_width 0.508))
    (polygon
      (pts
        (xy 74.525 47.2) (xy 85.5 47.2) (xy 85.225 45.975) (xy 84.45 44.925) (xy 83.45 44.35)
        (xy 82.65 44.2) (xy 77.5 44.175) (xy 76.175 44.5) (xy 75.325 45.125) (xy 74.775 45.875)
        (xy 74.55 46.725)
      )
    )
  )
     */
}
