package dk.dren.kicad.cmd;

import dk.dren.kicad.pcb.*;
import dk.dren.kicad.primitive.Point;
import org.decimal4j.immutable.Decimal6f;
import picocli.CommandLine;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

@CommandLine.Command(name = "roundover", description = "Creates mouse-bites out of Eco1.User lines where they cross Edge.Cuts")
public class Roundover implements Callable<Integer> {
    @CommandLine.ParentCommand
    PCBCmd pcbCmd;

    @CommandLine.Option(
            names = {"--fillet"},
            description = "The radius of the corners, \n" +
                    "Default is 1.3 mm to accommodate the typical 2.5 mm router bit"
    )
    Decimal6f filletRadius = Decimal6f.valueOf(1.3);
    private Point point;

    public static final Decimal6f EDGE_CUT_LINE_WIDTH = Decimal6f.valueOf(0.1);

    @Override
    public Integer call() throws Exception {
        Board board = pcbCmd.getBoard();

        List<LineNode> replacementLines = new ArrayList<>();
        Set<LineNode> lines = board.lines(LineNode.EDGE_CUTS).collect(Collectors.toSet());
        while (!lines.isEmpty()) {

            LineNode line = lines.iterator().next();
            final LineNode first = line;
            point = line.getEnds().getEnd();

            while (!lines.isEmpty()) {
                LineNodeIntersection next = board.findEdgeCutLineConnectingTo(point);
                if (next == null) {
                    throw new IllegalArgumentException("Could not find next line from "+line.getEnds());
                }

                board.removeChild(line);

                replacementLines.add(createLine(point, next.getIntersection(), LineNode.EDGE_CUTS, EDGE_CUT_LINE_WIDTH));

                point = next.getIntersection();
                if (next.getLineNode() == first) {
                    break;
                }
            }

        }

        try (Writer writer = new FileWriter(new File(pcbCmd.getPcbFile().getParentFile(), "output."+pcbCmd.getPcbFile().getName()))) {
            KicadPcbSerializer.write(board, writer);
        }

        return 0;
    }

    private LineNode createReplacementLine(LineNodeIntersection old) {
        Point a = old.getLineNode().getEnds().getStart();
        Point b = old.getLineNode().getEnds().getEnd();

        return null;
    }

    private LineNode createLine(Point a, Point b, String layer, Decimal6f width) {
        return new LineNode(new Line(a, b), layer, width, null);
    }

}
