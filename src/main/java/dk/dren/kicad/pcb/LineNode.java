package dk.dren.kicad.pcb;

import dk.dren.kicad.primitive.Point;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.decimal4j.immutable.Decimal6f;

import java.util.ArrayList;
import java.util.Collection;

@AllArgsConstructor
@Getter
@Setter
public class LineNode implements Node {
    public static final String NAME = "gr_line";

    public static final String EDGE_CUTS = "Edge.Cuts";
    public static final String ECO1_USER = "Eco1.User";
    public static final String ECO2_USER = "Eco2.User";
    public static final String FRONT_COURTYARD = "F.CrtYd";
    public static final String FRONT_SILK_SCREEN = "F.SilkS";
    public static final String FRONT_FABRICATION = "F.Fab";

    public static final String LAYER = "layer";
    public static final String WIDTH = "width";
    public static final String TSTAMP = "tstamp";

    private Line ends;
    private String layerName;
    private Decimal6f width;
    private String tstamp;

    public LineNode(RawNode rawNode) {
        this.ends = Line.of(rawNode);
        layerName = rawNode.getPropertyValue(LAYER).toString();
        // Strip " off layerName start and end.
        if (layerName.startsWith("\"")) {
            layerName = layerName.substring(1);
        }
        if (layerName.endsWith("\"")) {
            layerName = layerName.substring(0,layerName.length() - 1);
        }
        
        width = rawNode.getPropertyValue(WIDTH).getDecimal6f();

        Node tsp = rawNode.getProperty(TSTAMP);
        if (tsp != null) {
            tstamp = tsp.getAttributes().get(0).toString();
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Collection<NodeOrValue> getAllChildren() {
        ArrayList<NodeOrValue> a = new ArrayList<>(ends.asNodes());
        a.add(new RawNode(LAYER, new MutableValue(layerName)));
        a.add(new RawNode(WIDTH, new MutableValue(width)));
        if (tstamp != null) {
            a.add(new RawNode(TSTAMP, new MutableValue(tstamp)));
        }
        return a;
    }
}
