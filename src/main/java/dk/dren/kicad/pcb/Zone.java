package dk.dren.kicad.pcb;

import lombok.Getter;
import lombok.experimental.Delegate;

@Getter
public class Zone implements Node {
    public static final String NAME = "zone";

    @Delegate
    private final RawNode rawNode;
    private final Polygon polygon;

    public Zone(RawNode rawNode) {
        this.rawNode = rawNode;
        polygon = (Polygon) getFirstChildByName(Polygon.POLYGON).get();
    }

    public String getLayer() {
        return getProperty("layer").getAttributes().get(0).getValue();
    }

}
